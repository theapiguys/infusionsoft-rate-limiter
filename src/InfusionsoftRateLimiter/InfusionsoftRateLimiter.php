<?php
/**
 * Created by The API Guys.
 * User: Brandon
 * Date: 9/27/2018
 * Time: 16:00
 */

namespace InfusionsoftRateLimiter;

use Infusionsoft\InfusionsoftException;
use Infusionsoft\Token;
use Infusionsoft\TokenExpiredException;
use InfusionsoftRateLimiter\Http;
use Infusionsoft\Infusionsoft;
use InfusionsoftRateLimiter\Http\GuzzleHttpClient;
use Psr\Log\LoggerInterface;


class InfusionsoftRateLimiter extends Infusionsoft
{
    /**
     * @var array
     */
    protected $lastResponseHeaders;
    /**
     * @var int
     */
    protected $throttle_percent = 80;
    /**
     * @var int
     */
    protected $throttle_sleep = 1;

    public function __construct(array $config = array(), LoggerInterface $httpLogAdapter = null)
    {
        parent::__construct($config);

        $this->setHttpLogAdapter($httpLogAdapter);
        $client = new GuzzleHttpClient($this->debug, $this->getHttpLogAdapter());
        $this->setHttpClient($client);
    }

    /**
     * @param Http\ClientInterface $client
     */
    public function setHttpClient($client)
    {
        $this->httpClient = $client;
    }

    /**
     * @return Http\SerializerInterface
     */
    public function getSerializer()
    {
        if ( ! $this->serializer) {
            return new Http\InfusionsoftSerializer();
        }

        return $this->serializer;
    }


    /**
     * @param string $code
     *
     * @return Token
     * @throws \Exception
     */
    public function requestAccessToken($code)
    {
        $params = array(
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'code' => $code,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->redirectUri,
        );

        $client = $this->getHttpClient();
        $this->checkRequestLock();
        try{
            list($tokenInfo, $this->lastResponseHeaders) = $client->request('POST', $this->tokenUri, [
                'body' => http_build_query($params),
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded']
            ]);

        }catch (\Exception $e){
            sleep($this->throttle_sleep);
            $this->checkRequestLock();
            list($tokenInfo, $this->lastResponseHeaders) = $client->request('POST', $this->tokenUri, [
                'body' => http_build_query($params),
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded']
            ]);
        }

        $this->setToken(new Token(json_decode($tokenInfo, true)));

        return $this->getToken();
    }

    /**
     * @return Token
     * @throws \Exception
     */
    public function refreshAccessToken()
    {
        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret),
            'Content-Type' => 'application/x-www-form-urlencoded'
        );

        $params = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->getToken()->getRefreshToken(),
        );

        $client = $this->getHttpClient();
        $this->checkRequestLock();

        try{

            list($tokenInfo, $this->lastResponseHeaders) = $client->request('POST', $this->tokenUri,
                ['body' => http_build_query($params), 'headers' => $headers]);

        }catch (\Exception $e){
            sleep($this->throttle_sleep);
            $this->checkRequestLock();

            list($tokenInfo, $this->lastResponseHeaders) = $client->request('POST', $this->tokenUri,
                ['body' => http_build_query($params), 'headers' => $headers]);
        }


        $this->setToken(new Token(json_decode($tokenInfo, true)));

        return $this->getToken();
    }

    /**
     * @throws InfusionsoftException
     * @return mixed
     * @throws \Exception
     */
    public function request()
    {
        // Before making the request, we can make sure that the token is still
        // valid by doing a check on the end of life.
        $this->checkRequestLock();
        $token = $this->getToken();
        if ($this->isTokenExpired()) {
            throw new TokenExpiredException;
        }

        $url = $this->url . '?' . http_build_query(array('access_token' => $token->getAccessToken()));

        $params = func_get_args();
        $method = array_shift($params);

        // Some older methods in the API require a key parameter to be sent
        // even if OAuth is being used. This flag can be made false as it
        // will break some newer endpoints.
        if ($this->needsEmptyKey) {
            $params = array_merge(array('key' => $token->getAccessToken()), $params);
        }

        // Reset the empty key flag back to the default for the next request
        $this->needsEmptyKey = true;

        $client = $this->getSerializer();

        try {
            $this->checkRequestLock();
            list($response, $this->lastResponseHeaders) = $client->request($method, $url, $params, $this->getHttpClient());
        } catch (\Exception $e) {
            $this->getHttpLogAdapter()->info("Exception on first call, waiting: " . $e->getMessage());
            $this->checkRequestLock();
            sleep($this->throttle_sleep);
            list($response, $this->lastResponseHeaders) = $client->request($method, $url, $params, $this->getHttpClient());
        }
        $this->checkRequestRate();

        return $response;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $params
     *
     * @throws InfusionsoftException
     * @return mixed
     * @throws \Exception
     */
    public function restfulRequest($method, $url, $params = array())
    {
        // Before making the request, we can make sure that the token is still
        // valid by doing a check on the end of life.
        $this->checkRequestLock();
        $token = $this->getToken();
        if ($this->isTokenExpired()) {
            throw new TokenExpiredException;
        }

        $client = $this->getHttpClient();
        $full_params = [];

        if (strtolower($method) === 'get' || strtolower($method) === 'delete') {
            $params = array_merge(array('access_token' => $token->getAccessToken()), $params);
            $url = $url . '?' . http_build_query($params);
        } else {
            $url = $url . '?' . http_build_query(array('access_token' => $token->getAccessToken()));
            $full_params['body'] = json_encode($params);
        }

        $full_params['headers'] = array(
            'Content-Type' => 'application/json',
        );

        try {
            $this->checkRequestLock();
            list($response, $this->lastResponseHeaders) = $client->request($method, $url, $full_params);
        } catch (\Exception $e) {
            $this->getHttpLogAdapter()->info("Exception on first call, waiting: " . $e->getMessage());
            $this->checkRequestLock();
            sleep($this->throttle_sleep);
            list($response, $this->lastResponseHeaders) = $client->request($method, $url, $full_params);
        }
        $this->checkRequestRate();
        return json_decode((string)$response, true);
    }

    public function setThrottlePercent($percent)
    {
        $this->throttle_percent = $percent;
    }

    public function setThrottleSleep($seconds)
    {
        $this->throttle_sleep = $seconds;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function checkRequestLock()
    {
        $running = 0;
        if (!file_exists(__DIR__ . "/quota.lock")) {
            $put = file_put_contents(__DIR__ . "/quota.lock", serialize(""));
            if ($put === false) {
                throw new \Exception("Cannot create lock file");
            }
        }
        while ($contents = unserialize(file_get_contents(__DIR__ . "/quota.lock"))) {
            $this->getHttpLogAdapter()->info("Locked $running");

            $running++;
            if (empty($contents) || $running > 30) {
                $this->getHttpLogAdapter()->info("pass $running");
                return true;
            }

            $_date = $contents['date'];
            $now = new \DateTime('now');
            if($now->getTimestamp() - $_date->getTimestamp() > 1 ){
                file_put_contents(__DIR__ . "/quota.lock", serialize(""));
            }

            sleep(1);
        }
        return true;
    }

    public function checkRequestRate()
    {
        $alloted_qps = $this->getLastResponseHeaderByKey("X-Plan-QPS-Allotted") ?: $this->getLastResponseHeaderByKey("X-PackageKey-QPS-Allotted");
        $current_qps = $this->getLastResponseHeaderByKey("X-Plan-QPS-Current") ?: $this->getLastResponseHeaderByKey("X-PackageKey-QPS-Current");

        $percentage = $alloted_qps ? ($current_qps / $alloted_qps) * 100 : 0;
        $this->getHttpLogAdapter()->info("Current: {$current_qps} Limit: {$alloted_qps} Percentage: {$percentage}");

        $data = [
            "date" => new \DateTime('now'),
            "alloted_qps" => $alloted_qps,
            "current_qps" => $current_qps,
            "used_percentage" => $percentage,
        ];
        if ($percentage >= $this->throttle_percent) {
            $this->getHttpLogAdapter()->info("Sleeping for {$this->throttle_sleep} seconds");
            file_put_contents(__DIR__ . "/quota.lock", serialize($data));
            sleep($this->throttle_sleep);
            file_put_contents(__DIR__ . "/quota.lock", serialize(""));
        }
    }

    /**
     * @return array
     */
    public function getLastResponseHeaders()
    {
        return $this->lastResponseHeaders;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getLastResponseHeaderByKey($key)
    {
        if (!isset($this->lastResponseHeaders[$key])) {
            return null;
        }
        if (!is_array($this->lastResponseHeaders[$key])) {
            $this->lastResponseHeaders[$key] = [];
        }
        if (count($this->lastResponseHeaders[$key]) == 1) {
            return $this->lastResponseHeaders[$key][0];
        }
        return $this->lastResponseHeaders[$key];
    }
}